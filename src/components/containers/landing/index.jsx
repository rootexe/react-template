import React, { memo } from "react";
import { FormattedMessage } from "react-intl";
import { Tag } from "@blueprintjs/core";
import styled from "styled-components";
import breakpoint from "styled-components-breakpoint";

import Button from "../../modules/button";

const Container = styled.div`
    width: 80%;
    background-color: ${props => props.theme.colors.secondary};
    margin: 0 auto;

    ${breakpoint("desktop")`
      width: 80%;
    `}

    ${breakpoint("tablet")`
      width: 90%;
    `}

    ${breakpoint("mobile")`
      width: 100%;
    `}
`;

const BtnContainer = styled.div`
  width: 100px;
  height: 100px;
  border: 1px solid white;
  padding: 25px;
`;

const GreetingBtn = styled(Button)`
  font-weight: 700;
`;

export default memo(() => (
  <Container>
    <Tag>
      <FormattedMessage id="landing" />
    </Tag>
    <BtnContainer>
      <GreetingBtn />
    </BtnContainer>
  </Container>
));
