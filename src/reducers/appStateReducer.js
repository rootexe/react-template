import types from "./../actions/appState/types";

const defaultValue = {
  loading: true
};

export default function(state = defaultValue, action) {
  switch (action.type) {
    case types.APPSTATE_LOADING:
      return {
        ...state,
        loading: action.payload
      };
    default:
      return state;
  }
}
