export class Color {
  constructor(name, hex) {
    this.name = name;
    this.hex = hex;
  }

  getHex = () => this.hex;

  toString = () => `var(--color-${this.name})`;
}

const colors = colorObj => {
  const keys = Object.keys(colorObj);
  const newColorObj = {};
  for (let i = 0; i < keys.length; i++) {
    const _color = keys[i];
    newColorObj[_color] = new Color(_color, colorObj[_color]);
  }

  return newColorObj;
};

export { colors };

export default {
  white: "#ffffff",
  black: "#000000",
  chestnutRose: "#cd5360"
};
